# LaTeX

Repository to build different documents in LaTeX

## Requirements Linux
- [Install Docker Debian](https://docs.docker.com/engine/install/debian/)
- [Install Docker Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
- [Install Docker CentOS](https://docs.docker.com/engine/install/centos/)
- Install make Debian/Ubuntu:
```console
sudo apt install -y make
```
- Install make CentOS:
```console
yum install -y make
```
## Requirements Windows (not fully tested)
- [Install Docker Windows](https://docs.docker.com/desktop/windows/install/)

## Build Docker Container
### Linux
```console
make docker_build
```
### Windows
to be added

## How to create new document directory
Copy the folder template to new folder inside documents. For example:
```console
cp -r documents/template documents/<document_name>
```
If for the new document it needs a different cover or a different index it should be added to the folder cover/index with a different name. After that, it modifies the lines in the file **main.tex**:
```latex
\input{../../cover/<new_cover_file>.tex}
\input{../../index/<new_index_file>.tex}
```
To compile the pdf file:
```console
make <document_name>.pdf
```

## How to add the new document to CI/CD
```yml
<document_name>_build:
  extends: .build_documents
  variables:
    PDF: <document_name>
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
    - if: '$CI_COMMIT_TAG =~ /^v\d+.\d+.\d+/'
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - Makefile
        - documents/<document_name>/*.tex
        - documents/<document_name>/**/*.tex
```

## Maintainers
- [Alén Arias Vázquez](mailto:alen.arias.vazquez@cern.ch)
