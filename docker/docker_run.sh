#!/usr/bin/env sh

REPO_PATH=$(git rev-parse --show-toplevel)

docker run \
    -v ${HOME}:${HOME} \
    -v /etc/passwd:/etc/passwd:ro \
    -v /etc/shadow:/etc/shadow:ro \
    -v /etc/group:/etc/group:ro \
    -v /tmp:/tmp \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v ${HOME}/.bashrc:${HOME}/.bashrc \
    -v /var/run/dbus:/var/run/dbus \
    -v /usr/share/git/completion:/usr/share/git/completion \
    -h $(hostname) \
    --mac-address="a0:00:27:00:00:2a" \
    -e DISPLAY=$DISPLAY \
    --privileged \
    -i -w $PWD -t -u $(id -u):$(id -g) --rm \
    -e PATH=${REPO_PATH}/utils:${PATH}:/bin \
    $IMAGE \
    /bin/bash
