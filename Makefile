##!/usr/bin/make
################################################################################
# DOCKER
DOCKER_NAME=docker-latex:latest
DOCKERFILE_PATH=docker

docker_build:
	docker build -f ${DOCKERFILE_PATH}/Dockerfile -t ${DOCKER_NAME} ${DOCKERFILE_PATH}

################################################################################
# CMD for call docker
DOCKER_CALL= docker run -v ${HOME}:${HOME} \
            -w${CURDIR} \
            -v${CURDIR}:${CURDIR} \
            -v /etc/passwd:/etc/passwd:ro \
            -v /etc/shadow:/etc/shadow:ro \
            -v /etc/group:/etc/group:ro \
            -v /tmp:/tmp \
            -v /tmp/.X11-unix:/tmp/.X11-unix \
            -v /var/run/dbus:/var/run/dbus \
            -u $$(id -u):$$(id -g) \
            --privileged \
            -t ${DOCKER_NAME} /bin/bash -c

################################################################################
# CI rule
ifdef GITLAB_CI
    CMD=eval
else
    CMD=${DOCKER_CALL}
endif

################################################################################
# Git & Latex iteration
GIT_TAG=$(shell git describe --tags --abbrev=0 --dirty=-D 2>/dev/null || echo "v0.0.0")

################################################################################
# Project list
MAIN_PATH=documents
SRC_PATH:=$(shell find ${MAIN_PATH} -maxdepth 2 -name main.tex)
SRC_BUILD_LIST:=$(patsubst %/,%,$(dir $(SRC_PATH)))
SRC_LIST:=$(patsubst ${MAIN_PATH}/%,%,$(SRC_BUILD_LIST))
LIST_COMPILE:=$(addsuffix .pdf, ${SRC_LIST})
LIST_CLEAN:=$(addsuffix .clean, ${SRC_LIST})

################################################################################
# Build rules
OUTPUT_PATH=output_pdf

${OUTPUT_PATH}:
	mkdir -p $@

${LIST_COMPILE}: ${OUTPUT_PATH}
	${CMD} 'cd ${MAIN_PATH}/$(basename $@) && latexmk -pdf -jobname=$(basename $@) -halt-on-error main.tex'
	${CMD} 'mkdir -p ${OUTPUT_PATH}/$(basename $@)'
	${CMD} 'cp ${MAIN_PATH}/$(basename $@)/$(basename $@).pdf ${OUTPUT_PATH}/$(basename $@)/'

all: ${LIST_COMPILE}

################################################################################
# Clean rules
${LIST_CLEAN}:
	${CMD} 'rm -rf ${OUTPUT_PATH}/$(basename $@)'
	${CMD} 'rm -rf ${MAIN_PATH}/$(basename $@)/{*.aux,*.log,*.nav,*.snm,*.toc,*.out,*.pdf,*.fdb_latexmk,*.fls}'
	${CMD} 'rm -rf ${MAIN_PATH}/$(basename $@)/{*.maf,*.mtc,*.mtc0,*.lot,*.lof,*.bbl,*.blg}'

clean:
	${CMD} 'rm -rf ${OUTPUT_PATH}'
	${CMD} 'rm -rf ${MAIN_PATH}/*/{*.aux,*.log,*.nav,*.snm,*.toc,*.out,*.pdf,*.fdb_latexmk,*.fls}'
	${CMD} 'rm -rf ${MAIN_PATH}/*/{*.maf,*.mtc,*.mtc0,*.lot,*.lof,*.bbl,*.blg}'
